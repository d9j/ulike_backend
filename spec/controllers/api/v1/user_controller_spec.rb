require 'rails_helper'
require 'faker'
include ActionDispatch::TestProcess
RSpec.describe Api::V1::UserController, :type => :controller do
  describe "Basic actions" do
    before(:all) do
      query_log
    end

    def post_user
      @user = FactoryGirl.attributes_for(:user)
      @user[:password] =Faker::Internet.password
      @user
    end
    it "Should Create a new user" do
      expect{
        post :create, :user => post_user
      }.to change(User,:count).by(1)
      expect(UserProfile.count).to eq(1)
      expect(AuthHolder.count).to eq(1)
      resp =  parse_json(response)
      expect(resp['response']).to eq(I18n.t('user_account.create_successful'))
    end
  end
end