require 'faker'
include ActionDispatch::TestProcess
FactoryGirl.define do
  factory :user,:class => User  do
    phone Faker::PhoneNumber.phone_number
    email Faker::Internet.email('JohnSmith')
    password  Faker::Internet.password(15)
  end
  trait :user_with_images do
    after(:create) do |u|
      (0..3).each do
        FactoryGirl.create(:user_images, user: u)
      end
    end
  end
end