class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :phone,limit: 30, null: false
      t.string :email,limit: 255, null: false
      t.string :password_hash
      t.string :password_salt
      t.timestamps null: false
    end
    add_index(:users,:email, :unique => true)
    add_index(:users,:phone, :unique => true)
  end
end
