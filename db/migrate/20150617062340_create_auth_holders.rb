class CreateAuthHolders < ActiveRecord::Migration
  def change
    create_table :auth_holders do |t|
      t.belongs_to :user
      t.string :client_hash
      t.string :private_hash
      t.datetime :expires_at
      t.timestamps
    end
  end
end
