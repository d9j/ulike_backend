class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.belongs_to :user
      t.string :first_name
      t.string :last_name
      t.timestamps null: false
    end
    add_index(:user_profiles,:user_id,:unique => true)
  end
end
