class Hasher
  def self.generate_hash
    first = SecureRandom.hex(rand(10..12)).to_i(16).to_s(36)
    second = SecureRandom.hex(rand(10..10)).to_i(16).to_s(36).upcase
    (first+second).split("").shuffle.join
  end
end