class  Api::V1::UserController < Api::BaseApiController

  # Post Create
  def create
  user = User.new(user_create_fields)
  unless user.save
      puts "Received  #{user.errors.inspect}" .colorize(:red)
     return render json: {:response => t('user_account.failed_create') , :result => user.errors.messages}
  end
   render json: {:response => t('user_account.create_successful') , :result => user.auth_data}
 end
  private
  def user_create_fields
    params.require(:user).permit(:email, :password, :phone)
  end

end