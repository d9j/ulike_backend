class User < ActiveRecord::Base
  has_one :user_profile, dependent: :destroy, inverse_of: :user
  has_many :auth_holders, dependent: :destroy
  attr_reader :auth_data
  attr_accessor :password
  #validates_confirmation_of :password
  validate :password_validator
  validates_presence_of :email
  validates_presence_of :phone
  validates_uniqueness_of :email
  validates_uniqueness_of :phone
  before_save :encrypt_password
  after_create :create_user_profile
  after_create :generate_auth_hash
  def self.new_from_params(params)
    user = User.new
    user.email = params[:email]
    user.phone = params[:phone]
    user.password = params[:password]
    user
  end
  def self.max_password_length
    200
  end
  def password_validator
    PasswordValidator.new(attributes: :password).validate_each(self, :password, @raw_password)
  end
  def password=(password)
    # special case for passwordless accounts
    @raw_password = password unless password.blank?
  end

  def password
     # so that validator doesn't complain that a password attribute doesn't exist
    ''
  end
  def encrypt_password
    if @raw_password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(@raw_password, password_salt)
    end
  end
  def create_user_profile
    UserProfile.create(user_id: id)
  end
  def generate_auth_hash
    res  = AuthHolder.create(user_id: id)
    @auth_data = {:hash=> res.hashed_results}
  end

  def params_data
    {
        :password => @raw_password,
        :email => email,
        :phone => phone
    }
  end
end
