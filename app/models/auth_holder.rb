class AuthHolder < ActiveRecord::Base
  belongs_to :user
  before_save :generate_hash

  def generate_hash
    self.client_hash = Hasher.generate_hash
    self.private_hash = Hasher.generate_hash
  end

  def hashed_results
    {
        :client_hash =>  self.client_hash,
        :private_hash =>  self.private_hash
    }
  end


end

=begin

  create_table "auth_holders", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.string   "client_hash",  limit: 255
    t.string   "private_hash", limit: 255
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

=end